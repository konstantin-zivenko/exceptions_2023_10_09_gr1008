from collections import Counter


def myfunc(word: str) -> int:
    return sum(Counter(str))


if __name__ == "__main__":
    cases = (
        ("asd", 3),
        ("aasse", 1),
        ("123456667yy", 6),
    )
    for arg, result in cases:
        assert myfunc(arg) == result, f"ERROR!! myfunc({arg}) = {myfunc(arg)}, but expected: {result}"
