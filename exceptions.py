class MyException(Exception):
    pass


x = 10

y = 0

try:
   z = (x / y)
   print("після ділення на 0")
except ZeroDivisionError:
    print("Обробляю помилку ZeroDivisionError!")
    raise MyException("це моє повідомлення")
except TypeError:
    print("Обробляю помилку TypeError!")
print("Продовжую працювати...")




